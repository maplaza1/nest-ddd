import { InvalidArgumentException } from "./InvalidArgumentException";
import { Dni } from "./Dni";
import { Email } from "./Email";


export class Student
{
    private id: number;
    private name: string;
    private dni: Dni;
    private email: Email;

    constructor(id: number, name: string, dni: Dni, email: Email)
    {
        if (
            name === undefined ||
            name.length > 255 ||
            name.length < 3
          ) {
            throw new InvalidArgumentException(
              "Student's name should have between 2 and 255 chars"
            );
          }

        this.id = id;
        this.name = name;
        this.dni = dni;
        this.email = email;
    }

    public getId(): number
    {
        return this.id;
    }
}