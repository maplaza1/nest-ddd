import { Course } from "./Course";

export abstract class CourseRepository {
    abstract save(course: Course): void;

    abstract nextIdentity(): number;

    abstract async all(name: string): Promise<Course[]>;

    abstract byId(courseId: number): Course;
}