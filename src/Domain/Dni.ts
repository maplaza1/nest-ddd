import { InvalidArgumentException } from "./InvalidArgumentException";

export class Dni
{
    private dniNumber: string;

    constructor(dniNumber: string) {
        if (dniNumber === undefined || !/^[0-9]{8}[A-Z]$/g.test(dniNumber)) {
            throw new InvalidArgumentException("Student's dni should have correct format");
          }
        this.dniNumber = dniNumber;
    }
}