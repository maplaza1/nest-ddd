import { Student } from "./Student";

export abstract class StudentRepository
{
    abstract save(student: Student): void;

    abstract nextIdentity(): number;

    abstract all(): Promise<Student[]>;

    abstract byId(studentId: number): Student; 
}