import { InvalidArgumentException } from "./InvalidArgumentException";
import { Enrollment } from "./Enrollment";

export class Course 
{
    private id: number;
    private name: string;
    private places: number;
    private enrollments: Enrollment[];

    constructor(id: number, name: string, places: number) {
        if (
            name === undefined ||
            name.length > 255 ||
            name.length < 3
          ) {
            throw new InvalidArgumentException(
              "Course's name should have between 2 and 255 chars",
            );
          }
      
          if (name === undefined || places > 8 || places < 1) {
            throw new InvalidArgumentException(
              'Courses should have between 1 and 8 places',
            );
          }

        this.id = id;
        this.name = name;
        this.places = places;
        this.enrollments = [];
    }

    public getId()
    {
        return this.id;
    }
    public getName()
    {
        return this.name;
    }

    public getPlaces()
    {
        return this.places;
    }

    public enroll(studentId: number)
    {
        if (this.enrollments.length === this.places) {
          throw new InvalidArgumentException("Course is full")
        }

        const result = this.enrollments.filter((enrollment: Enrollment) => {
          return enrollment.getStudentId() == studentId;
        })
        
        if (result.length !== 0) {
          throw new InvalidArgumentException("Student already enrolled")
        }

        this.enrollments.push(new Enrollment(this.id, studentId));
    }
}