export class Enrollment
{
    private courseId: number;
    private studentId: number;

    constructor(courseId: number, studentId: number)
    {
        this.courseId = courseId;
        this.studentId = studentId;
    }

    public getStudentId(): number
    {
        return this.studentId;
    }
}