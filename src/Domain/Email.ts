import { InvalidArgumentException } from "./InvalidArgumentException";

export class Email
{
    private email: string;

    constructor(email: string) {
        if (
            email === undefined ||
            !/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g.test(
              email,
            )
          ) {
            throw new InvalidArgumentException(
              "Student's email should have correct format",
            );
          }
        this.email = email;
    }
}