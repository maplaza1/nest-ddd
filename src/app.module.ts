import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { DDDController } from './ddd.controller';
import { CreateCourseUseCase } from './Application/CreateCourseUseCase';
import { CourseRepository } from './Domain/CourseRepository';
import { MySQLCourseRepository } from './Infrastructure/MySQLCourseRepository';
import { InMemoryCourseRepository } from './Infrastructure/InMemoryCourseRepository';
import { GetCoursesUseCase } from './Application/GetCoursesUseCase';
import { CreateStudentUseCase } from './Application/CreateStudentUseCase';
import { StudentRepository } from './Domain/StudentRepository';
import { InMemoryStudentRepository } from './Infrastructure/InMemoryStudentRepository';
import { GetStudentsUseCase } from './Application/GetStudentsUseCase';
import { EnrollStudentUseCase } from './Application/EnrollStudentUseCase';

@Module({
  imports: [],
  controllers: [AppController, DDDController],
  providers: [
    CreateCourseUseCase,
    GetCoursesUseCase,
    CreateStudentUseCase,
    GetStudentsUseCase,
    EnrollStudentUseCase,
    { provide: CourseRepository, useClass: InMemoryCourseRepository},
    { provide: StudentRepository, useClass: InMemoryStudentRepository},
  ],
})
export class AppModule {}
