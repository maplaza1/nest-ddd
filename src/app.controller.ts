import {
  Controller,
  Post,
  Body,
  BadRequestException,
  Query,
  Get,
  Param,
  NotFoundException,
} from '@nestjs/common';
import { createConnection } from 'typeorm';
import { ApiProperty, ApiQuery, ApiTags } from '@nestjs/swagger';
import { CreateCourseUseCase } from './Application/CreateCourseUseCase';
import { MySQLCourseRepository } from './Infrastructure/MySQLCourseRepository';
import { GetCoursesUseCase } from './Application/GetCoursesUseCase';
import { CreateStudentUseCase } from './Application/CreateStudentUseCase';
import { GetStudentsUseCase } from './Application/GetStudentsUseCase';
import { EnrollStudentUseCase } from './Application/EnrollStudentUseCase';

class CraeteCourseRequest {
  @ApiProperty({ example: 'Nuevo curso' })
  name: string;
  @ApiProperty({ example: 5 })
  places: number;
}

class CreateStudentRequest {
  @ApiProperty({ example: 'Víctor' })
  name: string;
  @ApiProperty({ example: '12345678J' })
  dni: string;
  @ApiProperty({ example: 'victor@ual.es' })
  email: string;
}

class EnrollStudentRequest {
  @ApiProperty({ example: 1 })
  studentId: number;
}

@Controller()
export class AppController {

  constructor(
    private useCase: CreateCourseUseCase, 
    private useCase2: GetCoursesUseCase,
    private createStudentUseCase: CreateStudentUseCase,
    private getStudentsUseCase: GetStudentsUseCase,
    private enrollStudentUseCase: EnrollStudentUseCase
  ) { }

  @ApiTags('Courses')
  @Post('/courses')
  async createCourse(@Body() req: CraeteCourseRequest): Promise<object> {
    try {
      const result = await this.useCase.execute(req.name, req.places);
      return { courseId: result };
    } catch (error) {
      throw new BadRequestException(error.message)
    }
  }

  @ApiTags('Courses')
  @Get('/courses')
  @ApiQuery({ name: 'name', required: false })
  async listCourses(@Query('name') name: string): Promise<object> {
    return { "data": await this.useCase2.execute(name) };
  }

  @ApiTags('Students')
  @Post('/students')
  async createStudent(@Body() req: CreateStudentRequest): Promise<object> {
    try {
      const studentId = this.createStudentUseCase.execute(req.name, req.dni, req.email);
      return { studentId: studentId };
    } catch (error) {
      throw new BadRequestException(error.message)
    }
  }

  @ApiTags('Students')
  @Get('/students')
  async listStudents(): Promise<object> {
    return { "data": await this.getStudentsUseCase.execute() };
  }

  @ApiTags('Courses')
  @Post('/courses/:courseId/enrollments')
  async enrollStudent(
    @Body() req: EnrollStudentRequest,
    @Param('courseId') courseId: number,
  ): Promise<object> {
    try {
      const result = await this.enrollStudentUseCase.execute(courseId, req.studentId);
      return { courseId: result };
    } catch (error) {
      throw new BadRequestException(error.message)
    }
  }
}
