import { Controller, Get } from '@nestjs/common';

class Currency {
  private isoCode: String;

  constructor(isoCode: String) {
    if (isoCode !== 'EUR' && isoCode !== 'USD') {
      throw new Error('Invalid Currency');
    }
    this.isoCode = isoCode;
  }

  public equals(currency: Currency): boolean {
    return this.isoCode == currency.isoCode;
  }
}

class Money {
  private amount: number;
  private currency: Currency;

  constructor(amount: number, currency: Currency) {
    this.amount = amount;
    this.currency = currency;
  }

  public equals(money: Money):boolean {
    return this.amount == money.amount && this.currency.equals(money.currency);
  }

  public add(amount: number): Money {
      return new Money(this.amount + amount, this.currency);
  }
}

// Aggregate Root
class Order {
    private orderId: number;
    private amount: number;
    private status: string;
    private lines: Array<OrderLine>;

    constructor(orderId) {
        this.orderId = orderId;
        this.amount = 0;
        this.status = 'DRAFT'
        this.lines = [];
    }

    addLine(lineId, amount) {
        this.lines.push(new OrderLine(this.orderId, lineId, amount))
        this.amount += amount;
    }
}

class OrderLine {
    private orderId: number;
    private lineId: number;
    private amount: number;

    constructor(orderId, lineId, amount) {
        this.orderId = orderId;
        this.lineId = lineId;
        this.amount = amount;
    }

    public getAmount() {
        return this.amount;
    }
}

abstract class OrderRepository
{
   abstract save(order: Order);
   abstract byId(orderId): Order;

   // byEmail()
   // all()

} 

class oracleOrderResptiory extends OrderRepository
{
    save(order: Order) {
        // INSERT asdff
    }
    byId(orderId: any): Order {
        // SELECT
        return null;
    }
}

class redisOrderRepository  extends OrderRepository
{
    save(order: Order) {
        // HSET
    }
    byId(orderId: any): Order {
        // HGET
        return null;
    }
    
}

@Controller()
export class DDDController {
  @Get('/vo')
  public vo() {
    const myteneuros = new Money(10, new Currency('EUR'));

    this.logicaComplicada(myteneuros);
    console.log(myteneuros)
    const yourteneuros = new Money(10, new Currency('EUR'));

    const newOrder = new Order(123414);

    newOrder.addLine( 1, 150);

   // db.persist(newOrder);



    console.log(newOrder);



    return myteneuros.equals(yourteneuros);
  }

  private logicaComplicada(money: Money) {
      const newMoney = money.add(5);
  }
}
