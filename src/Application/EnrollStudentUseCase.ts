import { CourseRepository } from "../Domain/CourseRepository";
import { InvalidArgumentException } from "../Domain/InvalidArgumentException";
import { StudentRepository } from "../Domain/StudentRepository";
import { Injectable } from "@nestjs/common";

@Injectable()
export class EnrollStudentUseCase
{
    private courses: CourseRepository;
    private students: StudentRepository;

    constructor(courses: CourseRepository, students: StudentRepository) {
        this.courses = courses;
        this.students = students;
    }

    public execute(courseId: number, studentId: number)
    {
        // Arrange
        const course = this.courses.byId(courseId);
        if (course === null)
        {
            throw new InvalidArgumentException("Course not found");
        }
        const student = this.students.byId(studentId);
        if (student === null)
        {
            throw new InvalidArgumentException("Student not found");
        }
        // Act
        course.enroll(studentId);

        // assert
        this.courses.save(course);
    }
}