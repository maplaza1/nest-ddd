import { Injectable } from "@nestjs/common";
import { StudentRepository } from "../Domain/StudentRepository";
import { Student } from "../Domain/Student";
import { Dni } from "src/Domain/Dni";
import { Email } from "src/Domain/Email";

@Injectable()
export class CreateStudentUseCase
{
    private students: StudentRepository;

    constructor(students: StudentRepository) {
        this.students = students;
    }

    public execute(name: string, dni: string, email: string)
    {
        const studentId = this.students.nextIdentity();

        const student = new Student(studentId, name, new Dni(dni), new Email(email));

        this.students.save(student);

        return studentId;
    }
}