import { CreateCourseUseCase } from "./CreateCourseUseCase"
import { CourseRepository } from "../Domain/CourseRepository";

class FakeCourseRepository extends CourseRepository
{
    private nextIdentityValue = 1;
    private lastSaved;

    changeFutureIde(id) {
        this.nextIdentityValue = id;
    }
    getLastSaved() :import("../Domain/Course").Course{
        return this.lastSaved;
    }
    save(course: import("../Domain/Course").Course): void {
        this.lastSaved = course;
    }
    nextIdentity(): number {
        return this.nextIdentityValue;
    }
    all(name: string): Promise<import("../Domain/Course").Course[]> {
        throw new Error("Method not implemented.");
    }
    byId(courseId: number): import("../Domain/Course").Course {
        throw new Error("Method not implemented.");
    }
}

describe("Create course", () => {
    it("should fail if name with less than 3 chars", async () => {
        const useCase = new CreateCourseUseCase(new FakeCourseRepository());
        try {
            await useCase.execute("ab", 8);
        } catch (error) {
            expect(error.message).toBe("Course's name should have between 2 and 255 chars")
        }
    })
    it("should fail if name with more than 255 chars", async () => {
        const useCase = new CreateCourseUseCase(new FakeCourseRepository());
        try {
            await useCase.execute("absdaf".repeat(300), 8);
        } catch (error) {
            expect(error.message).toBe("Course's name should have between 2 and 255 chars")
        }
    })
    it("should fail if places with less than 1", async () => {
        const useCase = new CreateCourseUseCase(new FakeCourseRepository());
        try {
            await useCase.execute("absdaf", 0);
        } catch (error) {
            expect(error.message).toBe("Courses should have between 1 and 8 places")
        }
    })
    it("should fail if places with more than 8", async () => {
        const useCase = new CreateCourseUseCase(new FakeCourseRepository());
        try {
            await useCase.execute("absdaf", 10);
        } catch (error) {
            expect(error.message).toBe("Courses should have between 1 and 8 places")
        }
    })
    it("should save in database if everything is correct", async () => {
        const fakeRepo = new FakeCourseRepository();
        fakeRepo.changeFutureIde(59);
        const useCase = new CreateCourseUseCase(fakeRepo);
        await useCase.execute("absdaf", 2);

        expect(fakeRepo.getLastSaved().getId()).toBe(59)
        expect(fakeRepo.getLastSaved().getName()).toBe("absdaf")
        expect(fakeRepo.getLastSaved().getPlaces()).toBe(2)
        
    })
})