import { CourseRepository } from "../Domain/CourseRepository";
import { Course } from "../Domain/Course";
import { Injectable } from "@nestjs/common";
import { InvalidArgumentException } from "../Domain/InvalidArgumentException";

@Injectable()
export class CreateCourseUseCase
{
    private courses: CourseRepository;

    constructor(courses: CourseRepository) {
        this.courses = courses;
    }

    public async execute(name: string, places: number)
    {
        const courseId = this.courses.nextIdentity();
        const course = new Course(courseId, name, places);
        this.courses.save(course);
      
        return courseId;
    }
}