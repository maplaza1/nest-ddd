import { Injectable } from "@nestjs/common";
import { CourseRepository } from "../Domain/CourseRepository";
import { Course } from "../Domain/Course";

@Injectable()
export class GetCoursesUseCase
{
    constructor(private courses: CourseRepository) {}

    public execute(name:string): Promise<Course[]>
    {
        return this.courses.all(name);
    }
}