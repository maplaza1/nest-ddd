import { Injectable } from "@nestjs/common";
import { StudentRepository } from "../Domain/StudentRepository";
import { Student } from "../Domain/Student";

@Injectable()
export class GetStudentsUseCase
{
    constructor(private students: StudentRepository) {}

    public execute(): Promise<Student[]>
    {
        return this.students.all();
    }
}