import { CreateCourseUseCase } from "./CreateCourseUseCase"
import { CourseRepository } from "../Domain/CourseRepository";
import { StudentRepository } from "../Domain/StudentRepository";
import { EnrollStudentUseCase } from "./EnrollStudentUseCase";
import { Course } from "../Domain/Course";
import { Student } from "../Domain/Student";
import { Dni } from "../Domain/Dni";
import { Email } from "../Domain/Email";

class FakeCourseRepository extends CourseRepository
{
    private nextById = null;
    private nextIdentityValue = 1;
    private lastSaved;

    changeFutureIde(id) {
        this.nextIdentityValue = id;
    }
    getLastSaved() :import("../Domain/Course").Course{
        return this.lastSaved;
    }
    save(course: import("../Domain/Course").Course): void {
        this.lastSaved = course;
    }
    nextIdentity(): number {
        return this.nextIdentityValue;
    }
    all(name: string): Promise<import("../Domain/Course").Course[]> {
        throw new Error("Method not implemented.");
    }
    byId(courseId: number): import("../Domain/Course").Course {
        return this.nextById;
    }
    setNextById(next)
    {
        this.nextById = next
    }


}

class FakeStudentRepository extends StudentRepository
{
    private nextById = null;
    save(student: import("../Domain/Student").Student): void {
        throw new Error("Method not implemented.");
    }
    nextIdentity(): number {
        throw new Error("Method not implemented.");
    }
    all(): Promise<import("../Domain/Student").Student[]> {
        throw new Error("Method not implemented.");
    }
    byId(studentId: number): import("../Domain/Student").Student {
        return this.nextById;
    }
    setNextById(next)
    {
        this.nextById = next
    }

}
describe("Enroll student", () => {
    it('fails if course is full', () => {
        // arrange
        const courses = new FakeCourseRepository();
        const students = new FakeStudentRepository();
        const useCase = new EnrollStudentUseCase(courses, students);

        const testCourse = new Course(3, "aaa", 2);
        testCourse.enroll(4);
        testCourse.enroll(5);
        courses.setNextById(testCourse);

        const testStudent = new Student(8, "victor", new Dni("12345678J"), new Email("victor@ual.es"))
        students.setNextById(testStudent);

        // act
        let errorMsg = null;

        try {
            useCase.execute(1, 8);
        } catch (error) {
            errorMsg = error.message
        }
        // assert
        expect(errorMsg).toBe("Course is full")
    })
  



})