import { CourseRepository } from "../Domain/CourseRepository";
import { createConnection } from "typeorm";
import { Course } from "../Domain/Course";

export class MySQLCourseRepository extends CourseRepository
{
    byId(courseId: number): Course {
      throw new Error("Method not implemented.");
    }
    async all(name: any): Promise<Course[]> {
      const conn = await this.getConnection();

      let sql = 'SELECT * FROM courses';
      let params = [];

    if (name !== undefined) {
        sql += ' WHERE name LIKE ?';
        params.push(`%${name}%`);
      }

      const result = await conn.query(sql, params);

      conn.close();
      console.log(result);

      const mappedResult = result.map((row) => {
        return new Course(row.id, row.name, row.places);
      })

      return new Promise((resolve) => {
        resolve(mappedResult);
    });
    }
    id = 40;

    nextIdentity(): number {
       // query sequence
       return this.id++;
    }

    async save(course: Course) {
        const conn = await this.getConnection();
      
          const result = await conn.query(
            'INSERT INTO courses (id, name, places) VALUES (?, ?, ?)',
            [course.getId(), course.getName(), course.getPlaces()],
          );
      
          conn.close();

          return result.insertId;
    }
    
    getConnection() {
        return createConnection({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: 'example',
          database: 'ual',
        });
      }
      
}