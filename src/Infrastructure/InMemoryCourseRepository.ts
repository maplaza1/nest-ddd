import { CourseRepository } from "../Domain/CourseRepository";
import { Course } from "../Domain/Course";

export class InMemoryCourseRepository extends CourseRepository
{
    byId(courseId: number): Course {
        const result = this.courses.filter((course: Course) => {

            return course.getId() == courseId;
        })

        return result.length !== 0 ? result[0] : null
    }

    private courses = [];
    private id = 1;

    async all(name: any): Promise<Course[]> {
        return new Promise((resolve) => {
            resolve(this.courses);
        });
    }

    save(course: Course) {
        this.courses.push(course)
    }

    nextIdentity(): number {
        return this.id++;
    }
    
}