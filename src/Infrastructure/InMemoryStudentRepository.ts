import { StudentRepository } from "../Domain/StudentRepository";
import { Student } from "../Domain/Student";

export class InMemoryStudentRepository extends StudentRepository
{
    byId(studentId: number): Student {
        const result = this.students.filter((student: Student) => {
            return student.getId() == studentId;
        })

        return result.length !== 0 ? result[0] : null
    }
    private students = [];
    private id = 1;

    all(): Promise<Student[]> {
        return new Promise((resolve) => {
            resolve(this.students);
        });
    }

    save(student: Student) {
        this.students.push(student)
    }

    nextIdentity(): number {
        return this.id++;
    }
}